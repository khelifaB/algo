#include<stdlib.h>
#include<stdio.h>
#include <gmp.h>

int main( int argc, char* args[]){
  if(argc < 3){
    printf("Usage: %s <integer> <integer> \n",args[0]);
    exit(EXIT_FAILURE);
  }

  mpz_t p;
  mpz_init (p);
  if(mpz_set_str(p, args[1], 10) == -1){
    printf("Error : mpz_set_str(p, %s, 10) = -1\n",args[1]);
    exit(EXIT_FAILURE);
  }
  
  mpz_t n;
  mpz_init (n);
  if(mpz_set_str(n, args[2], 10) == -1){
    printf("Error : mpz_set_str(n, %s, 10) = -1\n",args[2]);
    exit(EXIT_FAILURE);
  }

  char *endptr;
  unsigned long int i, uli_n = strtoul(args[2], &endptr, 10);;

  mpz_t mod;
  mpz_init (mod);
  mpz_ui_pow_ui (mod, 10, uli_n);
  
  mpz_t result;
  mpz_init (result);
  mpz_set (result, n);
  
  for (i = 1; i < uli_n; i++){
    if(mpz_sgn(result)==0)
      break;
    mpz_powm (result, p, result, mod);
  }
    
  printf("%s^^%s [10^%s] = ",args[1],args[2],args[2]); mpz_out_str(stdout, 10, result);  printf("\n");

  return EXIT_SUCCESS;
}
